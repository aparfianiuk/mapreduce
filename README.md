Map-reduce method simulation (by multi-threading)
===================
The program considers the average rental of apartments in Minsk. Data is obtained from onliner.by.
In first case all data get by single thread. In second case data recived and handle by four threads.

Usage
-----
Clone project:

    $ git clone https://frombrest@bitbucket.org/frombrest/mapreduce.git

Build project:

    $ mvn package

And run:

    $ java -jar target/flats-0.1-jar-with-dependencies.jar