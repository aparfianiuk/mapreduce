import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.onliner.model.*;

public class Map {

    public static ArrayList<Integer> map(String URL){

        Document document = null;
        ArrayList<Integer> result = new ArrayList<>();

        try {
            document = getDocument(URL);
        } catch (UnirestException | IOException e) {
            System.out.println("Get Document Exception");
        }

        int pages = document.getPage().getLast();
        System.out.println("TOTAL: " + document.getTotal());

        for (int i = 1; i<=pages; i++){
            try {
                document = getDocument(URL+"&page="+i);
            } catch (UnirestException | IOException e) {
                System.out.println("Get Document Exception");
            }

            document.getApartments().stream().forEach(a -> result.add(Double.valueOf(a.getPrice().getAmount()).intValue()));
        }
        return result;
    }


    private static Document getDocument(String URL) throws IOException, UnirestException {
        ObjectMapper objectMapper = new ObjectMapper();
        Unirest.setDefaultHeader("Accept","\ttext/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        Unirest.setDefaultHeader("Accept-Encoding","gzip, deflate, br");
        String rawJSON = new Unirest().get(URL).asString().getBody();
        return objectMapper.readValue(rawJSON, Document.class);

    }

}
