import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import org.onliner.model.MyStructure;

public class Main {

    public static final String ONE_ROOM_REQUEST="https://ak.api.onliner.by/search/apartments?rent_type%5B%5D=1_room&only_owner=true&bounds%5Blb%5D%5Blat%5D=53.80551690917758&bounds%5Blb%5D%5Blong%5D=27.235107421875004&bounds%5Brt%5D%5Blat%5D=53.99000988393033&bounds%5Brt%5D%5Blong%5D=27.889480590820316&_=0.6510500352897356";
    public static final String TWO_ROOM_REQUEST="https://ak.api.onliner.by/search/apartments?rent_type%5B%5D=2_rooms&only_owner=true&bounds%5Blb%5D%5Blat%5D=53.80551690917758&bounds%5Blb%5D%5Blong%5D=27.235107421875004&bounds%5Brt%5D%5Blat%5D=53.99000988393033&bounds%5Brt%5D%5Blong%5D=27.889480590820316&_=0.06408286723114176";
    public static final String THREE_ROOM_REQUEST="https://ak.api.onliner.by/search/apartments?rent_type%5B%5D=3_rooms&only_owner=true&bounds%5Blb%5D%5Blat%5D=53.80551690917758&bounds%5Blb%5D%5Blong%5D=27.235107421875004&bounds%5Brt%5D%5Blat%5D=53.99000988393033&bounds%5Brt%5D%5Blong%5D=27.889480590820316&_=0.4563108541201373";
    public static final String FOUR_ROOM_REQUEST="https://ak.api.onliner.by/search/apartments?rent_type%5B%5D=4_rooms&rent_type%5B%5D=5_rooms&rent_type%5B%5D=6_rooms&only_owner=true&bounds%5Blb%5D%5Blat%5D=53.80551690917758&bounds%5Blb%5D%5Blong%5D=27.235107421875004&bounds%5Brt%5D%5Blat%5D=53.99000988393033&bounds%5Brt%5D%5Blong%5D=27.889480590820316&_=0.6843761095998345";
    public static final String ALL_ROOM_REQUEST="https://ak.api.onliner.by/search/apartments?rent_type%5B%5D=1_room&rent_type%5B%5D=2_rooms&rent_type%5B%5D=3_rooms&rent_type%5B%5D=4_rooms&rent_type%5B%5D=5_rooms&rent_type%5B%5D=6_rooms&only_owner=true&bounds%5Blb%5D%5Blat%5D=53.80551690917758&bounds%5Blb%5D%5Blong%5D=27.235107421875004&bounds%5Brt%5D%5Blat%5D=53.99000988393033&bounds%5Brt%5D%5Blong%5D=27.889480590820316&_=0.003606195644234478";

    public static final CountDownLatch COUNT_DOWN_LATCH = new CountDownLatch(4);
    public static final Semaphore SEMAPHORE = new Semaphore(1, true);

    public static final ArrayList<MyStructure> RESULT_SET = new ArrayList<>();

    private static int FINAL_RESULT = 0;
    private static long start = 0L;
    private static long end = 0L;

    public static void main(String[] args) throws InterruptedException {

     System.out.println("*** Single thread ***");
     start = System.nanoTime();

     //Map-reduce-reduce
     RESULT_SET.add(Reduce.reduce_flats(Map.map(ALL_ROOM_REQUEST)));
     FINAL_RESULT = Reduce.reduce_sets(RESULT_SET);

     end = System.nanoTime();
     System.out.println("AVG: " + FINAL_RESULT + "$ TIME: " + TimeUnit.NANOSECONDS.toMillis(end-start) + " ms.");

     RESULT_SET.clear();

     System.out.println("\n*** Multi thread ***");
     start = System.nanoTime();

     //Map-reduce in 4 flow
     new Worker(ONE_ROOM_REQUEST).start();
     new Worker(TWO_ROOM_REQUEST).start();
     new Worker(THREE_ROOM_REQUEST).start();
     new Worker(FOUR_ROOM_REQUEST).start();
     COUNT_DOWN_LATCH.await();

     //End reduce step in single flow
     FINAL_RESULT = Reduce.reduce_sets(RESULT_SET);

     end = System.nanoTime();
     System.out.println("AVG: " + FINAL_RESULT + "$ TIME: " + TimeUnit.NANOSECONDS.toMillis(end-start) + " ms.");

    }
}

class Worker extends Thread {
    private String url;

    Worker(String url){
        this.url=url;
    }

    public void run(){

        List<MyStructure> temp = new ArrayList<>();
        temp.add(Reduce.reduce_flats(Map.map(url)));
        try {
            Main.SEMAPHORE.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Main.RESULT_SET.addAll(temp);
        Main.SEMAPHORE.release();
        Main.COUNT_DOWN_LATCH.countDown();

    }
}
