package org.onliner.model;

/**
 * TODO: Document this class.
 */
public class MyStructure {
    private Integer price;
    private Integer count;

    public MyStructure() {
        price = 0;
        count = 0;
    }

    public Integer getPrice() {
        return this.price;
    }

    public void setPrice(final Integer price) {
        this.price = price;
    }

    public Integer getCount() {
        return this.count;
    }

    public void setCount(final Integer count) {
        this.count = count;
    }
}
