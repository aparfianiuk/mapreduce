package org.onliner.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "price",
    "rent_type",
    "location",
    "photo",
    "contact",
    "created_at",
    "last_time_up",
    "up_available_in",
    "url"
})
public class Apartment {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("price")
    private Price price;
    @JsonProperty("rent_type")
    private String rentType;
    @JsonProperty("location")
    private Location location;
    @JsonProperty("photo")
    private String photo;
    @JsonProperty("contact")
    private Contact contact;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("last_time_up")
    private String lastTimeUp;
    @JsonProperty("up_available_in")
    private Integer upAvailableIn;
    @JsonProperty("url")
    private String url;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("price")
    public Price getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Price price) {
        this.price = price;
    }

    @JsonProperty("rent_type")
    public String getRentType() {
        return rentType;
    }

    @JsonProperty("rent_type")
    public void setRentType(String rentType) {
        this.rentType = rentType;
    }

    @JsonProperty("location")
    public Location getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(Location location) {
        this.location = location;
    }

    @JsonProperty("photo")
    public String getPhoto() {
        return photo;
    }

    @JsonProperty("photo")
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @JsonProperty("contact")
    public Contact getContact() {
        return contact;
    }

    @JsonProperty("contact")
    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("last_time_up")
    public String getLastTimeUp() {
        return lastTimeUp;
    }

    @JsonProperty("last_time_up")
    public void setLastTimeUp(String lastTimeUp) {
        this.lastTimeUp = lastTimeUp;
    }

    @JsonProperty("up_available_in")
    public Integer getUpAvailableIn() {
        return upAvailableIn;
    }

    @JsonProperty("up_available_in")
    public void setUpAvailableIn(Integer upAvailableIn) {
        this.upAvailableIn = upAvailableIn;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "Apartment{" +
                "id=" + id +
                ", price=" + price +
                ", rentType='" + rentType + '\'' +
                ", location=" + location +
                ", photo='" + photo + '\'' +
                ", contact=" + contact +
                ", createdAt='" + createdAt + '\'' +
                ", lastTimeUp='" + lastTimeUp + '\'' +
                ", upAvailableIn=" + upAvailableIn +
                ", url='" + url + '\'' +
                '}';
    }
}
