package org.onliner.model;

import java.util.Objects;

public class Flat {

    private int id;
    private String price;
    private String type;
    private String URL;
    private String adress;
    private boolean isSended;

    public Flat(){

    }

    public Flat(Apartment a){
        id = a.getId();
        price = a.getPrice().getAmount() + a.getPrice().getCurrency();
        type = a.getRentType();
        URL = a.getUrl();
        adress = a.getLocation().getAddress();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public boolean isSended() {
        return isSended;
    }

    public void setSended(boolean sended) {
        isSended = sended;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flat flat = (Flat) o;
        return id == flat.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return URL;
    }
}
