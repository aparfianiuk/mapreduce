import java.util.List;
import org.onliner.model.MyStructure;

public class Reduce {

    public static MyStructure reduce_flats(List<Integer> prices){
        MyStructure result = new MyStructure();
        result.setCount(prices.size());
        result.setPrice(prices.stream().mapToInt(num -> num.intValue()).sum());
        return result;
    }

    public static int reduce_sets(List<MyStructure> prices){
        return (prices.stream().mapToInt(set -> set.getPrice()).sum())/(prices.stream().mapToInt(set -> set.getCount()).sum());
    }

}